#!/bin/bash
echo
 echo "************************************************************************"
 echo "**                                                                    **"
 echo "**                           NVIDIAINSTALL                            **"  
 echo "**                                                                    **"
 echo "************************************************************************"
echo
echo "Este es el script de Roberto Díaz, para instalar el driver privativo de nvidia en tu distribución" 
#leer readme de gitlab para obtener más información sobre la compatibilidad del script
echo
echo "Y es su 4º versión"
echo
sleep 3s
echo "Vamos a ello......"
echo ""
sleep 2s
echo  "Comprobaré que distribución usas e instalaré el driver de nvidia:"

#VERSION="grep '^(VERSION)=' /etc/os-release"
#NAME="grep '^(NAME)=' /etc/os-release"
#ID=grep '^(ID)=' /etc/os-release
NAME=$(egrep '^(NAME)=' /etc/os-release)
#echo El nombre es: $NAME
VERSION=$(egrep '^(VERSION)=' /etc/os-release)
#echo La versión es: $VERSION
ID=$(egrep '^(ID)=' /etc/os-release)

if [ "$ID" == "ID=debian" ]
then 
echo "Tu distribución es Debian"
sleep 1s
sudo apt install -y nvidia-detect ; nvidia-detect ; driver=$(nvidia-detect | grep "nvidia-" | sed 's/ //g' ) ; sudo apt install -y "$driver"
fi

if [ "$ID" == "ID=ubuntu" ]
then 
echo "Tu distribución es Ubuntu"
sleep 1s
sudo ubuntu-drivers autoinstall
fi

if [ "$ID" == "ID=linuxmint" ]
then 
echo "Tu distribución es Linux Mint"
sleep 1s
sudo ubuntu-drivers autoinstall
fi

if [ "$ID" == "ID=kdeneon" ]
then 
echo "Tu distribución es Kde Neón"
sleep 1s
sudo ubuntu-drivers autoinstall
fi

if [ "$ID" == "ID=manjaro" ]
then 
echo "Tu distribución es Manjaro"
sleep 1s
sudo mhwd -a pci nonfree 0300
fi

if [ "$ID" == "ID=fedora" ]
then
echo "Tu distribución es Fedora"
sleep 1s
sudo dnf install -y dnf-plugins-core
sudo dnf install -y \https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y \https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf update --refresh
sudo dnf install -y akmod-nvidia xorg-x11-drv-nvidia-cuda
fi

if [ "$ID" == "ID=arch" ]
then
echo "Tu distribución es Arch Linux"
sleep 1s
sudo pacman -Syu nvidia-dkms nvidia-settings --noconfirm
fi

if [ "$ID" == "ID=popos" ]
then
echo "Tu distribución es Pop! _OS"
sleep 1s
sudo apt install -y system76-driver-nvidia
fi

if [ "$ID" == "ID=zorinos" ]
then
echo "Tu distribución es Zorin OS"
sleep 1s
sudo ubuntu-drivers autoinstall
fi

echo
echo "Este script cumple las cuatro libertades del software libre, que son las siguientes:"
echo ""
echo "La libertad de ejecutar el software como te plazca y con cualquier objetivo. "
echo "La libertad de estudiar como funciona el programa y cambiarlo a tu gusto."
echo "La libertad de poder redistribuir copias del programa a los demás."
echo "La libertad de poder distribuir también tus mejoras al programa original."
sleep 3s
echo
echo
echo "Gracias por usar mi script"
echo
echo
echo "Ahora deberías reiniciar el sistema para que todos los modulos de nvidia se carguen en el kernel."
echo
sleep 3s

    read -p "¿Quieres reiniciar ahora (s/n)?" sn
    case $sn in
        [Ss]* )  sudo reboot;;
        [Nn]* ) exit;;
        * ) echo "Por favor, pulsa s o n.";;
    esac
done
