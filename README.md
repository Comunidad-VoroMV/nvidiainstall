# NVIDIAINSTALL

<p>Esto es un script sencillo para instalar el driver privativo de nvidia en tu distribución de GNU/Linux.

Las Distribuciones compatibles son las siguientes: 

<ul>
<li>Debian</li>
<li>Ubuntu</li>
<li>Linux Mint</li>
<li>KDE Neon</li>
<li>Manjaro</li>
<li>Fedora</li>
<li>Arch Linux</li>
<li>Pop!_OS</li>
<li>Zorin OS</li>
</ul>

</p>

# FUNCIONAMIENTO/INSTALACIÓN:

El script se ejecuta en una terminal con las siguientes ordenes que te indicaré abajo
<p></p>

```bash
chmod +x nvidiainstall.sh    #para dar permisos de ejecución al script
```

```bash
sudo ./nvidiainstall.sh     #Para ejecutar el script
```
<p></p>
<p><h3>NOTA IMPORTANTE:</h3> Debian 11 NO es compatible con algunos drivers de Nvidia como el 340 y versiones anteriores, esto implica que el script no sera capaz de instalar el driver ya que no esta incluido en Debian 11. Si quiere más información sobre el tema consulte la página del proyecto Debian que os dejo a continuación:

https://wiki.debian.org/NvidiaGraphicsDrivers

Se recomienda consultar la compatibilidad de la distribución en la documentación del proyecto.
</p>
